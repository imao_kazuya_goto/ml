import ml_setting
from usb_camera.camera import Camera
from comm.comm import Comm
from threading import Timer


if __name__ == '__main__':
    # センサー接続
    com_interface = Comm()
    com_interface.sensor_activate()

    # カメラ起動
    cam = Camera()

    while True:
        # センサー反応待ち(Serial Read)
        com_interface.sensor_wait()
        print("Sensor Responds.")

        # カメラ撮影および保存
        def capture():
            cam.capture_save()
            com_interface.send_result(b'1')

        Timer(ml_setting.SENSOR_DELAY, capture).start()
        print("Take Photo and Store it.")
