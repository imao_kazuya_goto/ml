import cv2
import datetime
import time
from threading import Thread


class Camera:
    def __init__(self):
        src = 'v4l2src device=/dev/video0 ' \
              'extra-controls=s,exposure_auto=1,exposure_absolute=20,power_line_frequency=2,' \
              'white_balance_temperature_auto=0,white_balance_temperature=4800 ' \
              '!image/jpeg, width=800, height=600, framerate=(fraction)60/1 ' \
              '!jpegdec !videoconvert ' \
              '!appsink max-buffers=1 drop=True'
        self.video_capture = cv2.VideoCapture(src, cv2.CAP_GSTREAMER)
        # '!image/jpeg, width=640, height=480, framerate=(fraction)61612/513 ' \

    def __del__(self):
        self.video_capture.release()

    def capture(self):
        ret, frame = self.video_capture.read()

        return frame

    def capture_save(self):
        frame = self.capture()

        file_name = f'data/{datetime.datetime.now().strftime("%Y-%m-%d-%H%M%S.%f")}.png'

        def write():
            cv2.imwrite(file_name, frame)

        thread = Thread(target=write)
        thread.start()


if __name__ == "__main__":
    cap = Camera()

    for i in range(5):
        print(i)
        cap.capture_save()
        time.sleep(1)
