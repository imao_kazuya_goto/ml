import datetime
import cv2

from nnabla.utils import nnp_graph

import ml_setting
from usb_camera.camera import Camera
from comm.comm import Comm
from threading import Timer


if __name__ == '__main__':
    nnp = nnp_graph.NnpLoader('network/results.nnp')
    print(nnp)

    graph = nnp.get_network("Eval", batch_size=1)
    print(graph)

    inputs = list(graph.inputs.values())
    x = inputs[0]

    outputs = list(graph.outputs.values())
    # Todo:出力名から結果変数を指定する
    y = outputs[3]

    # センサー接続
    com_interface = Comm()
    com_interface.sensor_activate()

    # カメラ起動
    cam = Camera()

    while True:
        # センサー反応待ち(Serial Read)
        com_interface.sensor_wait()
        print("Sensor Responds.")

        # カメラ撮影および検査
        def inspect():
            def send_ok():
                com_interface.send_result(b'1')

            def send_ng():
                com_interface.send_result(b'0')

            org_frame = cam.capture()

            # frame for the input of the network (resize and transpose for nnc)
            frame_400_300 = cv2.resize(org_frame, (400, 300)).transpose(2, 0, 1)

            # inspect
            x.d = frame_400_300.reshape(1, 3, 300, 400) * 1.0 / 255
            y.forward()

            diff_value = int(y.d[0][0] / 10000.0)

            if diff_value < 2000.0:
                result = "ok_" + str(diff_value)
                send_ok()
            else:
                result = "ng_" + str(diff_value)
                send_ng()

            # store frame with result named file
            file_name = f'data/{datetime.datetime.now().strftime("%Y-%m-%d-%H%M%S.%f")}-{result}.png'

            cv2.imwrite(file_name, org_frame)

        Timer(ml_setting.SENSOR_DELAY, inspect).start()
        print("Take photo and inspect it.")
