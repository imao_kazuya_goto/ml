import serial
from threading import Thread, Event
from common.throttle import throttle, TooSoon


class SensorNotAvailable(Exception):
    pass


class Comm(Thread):
    def __init__(self):
        super(Comm, self).__init__()
        self.daemon = True

        self.comm_addr = "/dev/ttyACM0"
        self.comm_baud = 9600

        self.ser = serial.Serial(self.comm_addr, self.comm_baud)

        self.event = Event()

    def sensor_activate(self):
        self.start()

    def run(self):
        while True:
            data = self.ser.readline()
            try:
                if is_sensor_enabled(data):
                    self.event.set()
            except TooSoon as e:
                print('Action ignored.', e)

    def sensor_wait(self):
        if self.is_alive():
            self.event.wait()
            self.event.clear()
        else:
            raise SensorNotAvailable("Sensor is not activated.")

    def send_result(self, result):
        self.ser.write(result)


@throttle(1)
def is_sensor_enabled(result):
    print("Sensor is enabled")
    return True
